package pl.sda.ldz24;


import javax.persistence.*;
import java.util.List;
@Entity
public class User extends BaseEntity {

    private String login;
    private String password;
    @OneToOne
    private Address address;
    @ManyToMany
    private List <Role> roles;

}
