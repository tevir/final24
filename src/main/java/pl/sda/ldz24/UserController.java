package pl.sda.ldz24;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/auth")
public class UserController {
    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/register")
    public String registerForm(Model model) {
        model.addAttribute("countries", Countries.values());
        model.addAttribute("registrationData",new UserRegistrationDTO());
        return "registrationPage"; //nazwa pliku html z /resources/templates
    }

    @PostMapping("/register")
    public String createNewUser(UserRegistrationDTO newUser) {
        System.out.println(newUser);
//        userService.addNewUser(newUser);
        return null;
    }
}
