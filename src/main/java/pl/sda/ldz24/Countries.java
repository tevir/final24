package pl.sda.ldz24;

import lombok.Getter;

@Getter
public enum Countries {
    POLAND ("Polska"),
    GERMANY ("Niemcy"),
    ITALY ("Włochy"),
    FRANCE("Francja");

    private String polishName;

    Countries(String polishName) {
        this.polishName = polishName;
    }
}
